/**
 * Created by Manuela Loaiza on 02/04/2016.
 */
/**
 * Created by vicman on 3/8/16.
 */
"use strict";
//https://restcountries.eu/rest/v1/alpha/co

$.ajax( "https://restcountries.eu/rest/v1/all" )
    .done(function(info) {
        for(var i=0; i< info.length; i++) {
            var data = info[i];
            console.log(data);
            $('.list-countries').append('' +
                '<p-country nombre="'+data.name+'" capital="'+data.capital+'" poblacion="'+data.population+'" imagen="assets/flags/'+ data.alpha2Code +'.png"></p-country>'
            );
        }
    })
    .fail(function(err) {
        console.log(err);
    });